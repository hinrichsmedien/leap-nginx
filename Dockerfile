FROM harbor.homenet/test/opensuse/leap:15.3
LABEL maintainer="Matthias Hinrichs <matthias.hinrichs@me.com>"
LABEL name="NGINX baseimage build profile"
LABEL version="1.0.0"

RUN zypper -n up && zypper -n install python python3 python-xml nginx

CMD nginx -g "daemon off;"